package com.river.managemusic

import android.app.AlertDialog
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val mListSong = ArrayList<Song>()
    val mSongAdapter = SongAdapter()

    lateinit var mediaPlayer: MediaPlayer
    lateinit var tvTotalTime: TextView
    lateinit var tvCurrentTime: TextView
    lateinit var seekbarController: SeekBar
    var handler = Handler()

    var isPlaying = false
    var isRandom = false
    var isReplay = false

    var isFirstClick = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //SUA DANH SACH BAI HAT O DAY
        // ĐẦU TIÊN BẠN PHẢI THÊM NHẠC VÀO TRONG APP.    CÁCH LÀM NHƯ SAU:
        // BẠN TẢI NHẠ VỀ VÀ COPY VAO CHỖ raw VÀ NHỚ ĐẶT TÊN VIẾT LIỀN, KHÔNG DẤU, KHÔNG VIẾT HOA

        // RỒI BẠN THÊM DÒNG LÊNH NHƯ BÊN DƯỚI
        // SỐ 1 2 3 KIA LÀ SỐ BÀI HÁT, SAU ĐÓ LÀ TÊN BÀI HÁT, TIẾP THEO LÀ CHỖ BẠN LƯU CÁI FILE NHẠC
        // CHÚNG TA LƯU Ở CHỖ raw
        // Mình ví dụ nhé:
        // ví dụ thế thôi. Bạn nhập chính xác nha

        // Bạn thêm danh sách bài hát như vậy là nó tự có tên bài hát trên danh sách rồi

        //DONE NHA

        mListSong.add(Song(1, "Buồn làm chi em ơi", R.raw.buon_lam_chi_em_oi))
        mListSong.add(Song(2, "Có chắc yêu là đây", R.raw.co_chac_yeu_la_day))
        mListSong.add(Song(3, "Để mị nói cho mà nghe", R.raw.de_mi_noi_cho_ma_nghe))
        mListSong.add(Song(4, "Duyên âm", R.raw.duyen_am))
        mListSong.add(Song(5, "Gác lại lo âu", R.raw.gac_lai_lo_au))
        mListSong.add(Song(6, "Kẻ cắp gặp bà già", R.raw.ke_cap_gap_ma_gia))

        mListSong.add(Song(7, "Anh Ta Bỏ Em Rồi", R.raw.anhtaboemroi))
        mListSong.add(Song(8, "Bigcityboi", R.raw.bigcity))
        mListSong.add(Song(9, "Bước Qua Đời Nhau", R.raw.buocquadoinhau))
        mListSong.add(Song(10, "Bức Tranh Yêu Thương", R.raw.buctranhyeuthuong))
        mListSong.add(Song(11, "Còn Gì Đau Hơn Chữ Đã Từng", R.raw.congidauhonchudatung))
        mListSong.add(Song(12, "Có Như Không Có", R.raw.conhukhongco))
        mListSong.add(Song(13, "Cứ Thế Rời Xa", R.raw.cutheroixa))
        mListSong.add(Song(14, "Day Dứt Nỗi Đau", R.raw.daydutnoidau))
        mListSong.add(Song(15, "Em Không Sai Chúng Ta Sai", R.raw.emkhongsaichungtasai))
        mListSong.add(Song(16, "Hết Thương Cạn Nhớ", R.raw.hetthuongcannho))
        mListSong.add(Song(17, "Không Sao Mà, Em Đây Rồi", R.raw.khongsaoma))
        mListSong.add(Song(18, "Không Thể Cùng Nhau Suốt Kiếp", R.raw.khongthecungnhausuotkiep))
        mListSong.add(Song(19, "Nước Mắt Em Lau Bằng Tình Yêu Mới", R.raw.nuocmatemlaubangtinhyeumoi))
        mListSong.add(Song(20, "OK", R.raw.ok))
        mListSong.add(Song(21, "Sao Anh Chưa Về Nhà", R.raw.saoanhchuave))
        mListSong.add(Song(22, "Thay Tôi Yêu Cô Ấy", R.raw.thaytoieyeucoay))
        mListSong.add(Song(23, "Tặng Anh Cho Cô Ấy", R.raw.tanganhchocoday))
        mListSong.add(Song(24, "Đen Đá Không Đường", R.raw.dendakhongduong))
        mListSong.add(Song(25, "Đừng Lo Anh Đợi Mà", R.raw.dungloanhdoima))

        rvSong.adapter = mSongAdapter
        rvSong.layoutManager = LinearLayoutManager(this)
        mSongAdapter.submitList(mListSong)

        tvTotalTime = findViewById(R.id.tv_total_time)
        tvCurrentTime = findViewById(R.id.tv_current_time)
        seekbarController = findViewById(R.id.seekbar_controller)

        mediaPlayer = MediaPlayer.create(this, mListSong[0].link)

        img_clock.setOnClickListener {
            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.input_time, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Turn off time:")
            val mAlertDialog = mBuilder.show()

            val buttonSave = mDialogView.findViewById<Button>(R.id.btLuuShow)
            val buttonCancel = mDialogView.findViewById<Button>(R.id.btHuyShow)

            buttonCancel.setOnClickListener {
                mAlertDialog.dismiss()
            }



            buttonSave.setOnClickListener {
                val thoiGian : EditText =  mDialogView.findViewById(R.id.et_nhap_thoi_gian)
                val tg = thoiGian.text.toString().toLong()*60000

                object : CountDownTimer(tg, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                    }

                    override fun onFinish() {
                        mediaPlayer.stop()
                        finish()
                    }
                }.start()

                mAlertDialog.dismiss()
            }
        }

        img_btn_play.setOnClickListener {
            if (isPlaying) {
                isPlaying = false
                img_btn_play.setImageResource(R.drawable.play_icon)
                mediaPlayer.pause()
            } else {
                if (isFirstClick) {
                    Toast.makeText(this, "Please choose a Song!", Toast.LENGTH_LONG).show()
                } else {
                    isPlaying = true
                    img_btn_play.setImageResource(R.drawable.pause_icon)
                    isFirstClick = false
                    mediaPlayer.start()
                }
            }
        }

        img_btn_random.setOnClickListener {
            if (isRandom) {
                isRandom = false
                img_btn_random.setImageResource(R.drawable.ic_random_white)
            } else {
                isRandom = true
                img_btn_random.setImageResource(R.drawable.ic_random_black)
                isReplay = false
                img_btn_replay.setImageResource(R.drawable.ic_replay_white)
            }
        }

        img_btn_replay.setOnClickListener {
            if (isReplay) {
                isReplay = false
                img_btn_replay.setImageResource(R.drawable.ic_replay_white)
            } else {
                isReplay = true
                img_btn_replay.setImageResource(R.drawable.ic_replay_black)
                isRandom = false
                img_btn_random.setImageResource(R.drawable.ic_random_white)
            }
        }

        var viTri = 0

        img_btn_next.setOnClickListener {
            img_btn_play.setImageResource(R.drawable.pause_icon)
            mediaPlayer.stop()
            if (viTri < mListSong.size - 1) {
                viTri++
            } else {
                viTri = 0
            }
            mediaPlayer = MediaPlayer.create(this, mListSong[viTri].link)
            tv_name_song.text = mListSong[viTri].title
            seekbarController.max = mediaPlayer.duration
            mediaPlayer.start()

            tvTotalTime.text = getTimeFormatted(mediaPlayer!!.duration)
        }

        img_btn_previous.setOnClickListener {
            img_btn_play.setImageResource(R.drawable.pause_icon)
            mediaPlayer.stop()
            if (viTri > 0) {
                viTri--
            } else {
                viTri = mListSong.size - 1
            }
            mediaPlayer = MediaPlayer.create(this, mListSong[viTri].link)
            tv_name_song.text = mListSong[viTri].title
            seekbarController.max = mediaPlayer.duration
            mediaPlayer.start()

            tvTotalTime.text = getTimeFormatted(mediaPlayer!!.duration)
        }

        rvSong.addOnItemTouchListener(
            RecyclerItemClickListener(
                this,
                rvSong,
                object : RecyclerItemClickListener.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {

                        tv_name_song.text = mListSong[position].title

                        isFirstClick = false

                        if (mediaPlayer != null){
                            mediaPlayer.stop()
                        }
                        img_btn_play.setImageResource(R.drawable.pause_icon)
                        isPlaying = true

                        viTri = position

                        mediaPlayer = MediaPlayer.create(view.context, mListSong[position].link)

                        seekbarController.max = mediaPlayer.duration
                        mediaPlayer.start()

                        tvTotalTime.text = getTimeFormatted(mediaPlayer!!.duration)

                        this@MainActivity.runOnUiThread(object : Runnable {
                            override fun run() {
                                if (mediaPlayer != null) {
                                    val mCurrentPosition: Int =
                                        mediaPlayer.currentPosition //clear ' /1000 '
                                    tvCurrentTime.text = getTimeFormatted(mCurrentPosition)
                                    seekbarController.progress = mediaPlayer.currentPosition

                                    if (getTimeFormatted(mCurrentPosition)
                                        == getTimeFormatted(mediaPlayer.duration)
                                    ) {


                                        if (isReplay) {
                                            mediaPlayer = MediaPlayer.create(
                                                view.context,
                                                mListSong[viTri].link
                                            )
                                            tv_name_song.text = mListSong[viTri].title
                                            seekbarController.max = mediaPlayer.duration
                                            mediaPlayer.start()
                                            tvTotalTime.text =
                                                getTimeFormatted(mediaPlayer!!.duration)
                                        } else if (isRandom) {
                                            if (viTri < mListSong.size - 2) {
                                                viTri += 2
                                            } else {
                                                viTri = 0
                                            }
                                            tv_name_song.text = mListSong[viTri].title
                                            mediaPlayer = MediaPlayer.create(
                                                view.context,
                                                mListSong[viTri].link
                                            )
                                            seekbarController.max = mediaPlayer.duration
                                            mediaPlayer.start()
                                            tvTotalTime.text =
                                                getTimeFormatted(mediaPlayer!!.duration)

                                        } else {
                                            if (viTri < mListSong.size - 1) {
                                                viTri++
                                            }
                                            else{
                                                viTri = 0
                                            }

                                            mediaPlayer = MediaPlayer.create(
                                                view.context,
                                                mListSong[viTri].link
                                            )
                                            tv_name_song.text = mListSong[viTri].title
                                            seekbarController.max = mediaPlayer.duration
                                            mediaPlayer.start()
                                            tvTotalTime.text =
                                                getTimeFormatted(mediaPlayer!!.duration)
                                        }
                                    }

                                }

                                handler.postDelayed(this, 1000)
                            }
                        })


                    }

                    override fun onItemLongClick(view: View?, position: Int) {
                        TODO("do nothing")
                    }
                })
        )

        seekbarController.setOnSeekBarChangeListener(object :
            OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                if (fromUser) {
                    mediaPlayer.seekTo(progress)
                    tvCurrentTime.text = getTimeFormatted(progress.toLong().toInt())
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

    }

    fun playNext(mp: MediaPlayer, view: View, position: Int, seeBar: SeekBar, total: TextView) {
        var mPlayer = mp
        mPlayer = MediaPlayer.create(view.context, mListSong[position + 1].link)

        seeBar.max = mPlayer.duration
        mPlayer.start()

        total.text = getTimeFormatted(mPlayer!!.duration)
    }

    private fun getTimeFormatted(milliSeconds: Int): String? {
        var finalTimerString = ""
        val secondsString: String

        val hours = (milliSeconds / 3600000)
        val minutes = (milliSeconds % 3600000) / 60000
        val seconds = (milliSeconds % 3600000 % 60000 / 1000)

        if (hours > 0) finalTimerString = "$hours:"

        secondsString = if (seconds < 10) "0$seconds" else "" + seconds
        finalTimerString = "$finalTimerString$minutes:$secondsString"

        return finalTimerString
    }

}