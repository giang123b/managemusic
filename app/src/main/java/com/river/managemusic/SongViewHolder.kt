package com.river.managemusic

import android.media.MediaPlayer
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_song.view.*

class SongAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val mListSong = ArrayList<Song>()
    private lateinit var mainView: View

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mainView = parent.rootView
        return SongViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_song,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = mListSong.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SongViewHolder).bindData(mListSong[position], mainView)

    }

    fun submitList(datas: ArrayList<Song>) {
        mListSong.clear()
        mListSong.addAll(datas)
        notifyDataSetChanged()
    }

}


class SongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var mediaPlayer: MediaPlayer
    lateinit var tvTotalTime : TextView
    lateinit var tvCurrentTime : TextView
    lateinit var seekbarController : SeekBar
    lateinit var runnable: Runnable
    lateinit var handler: Handler


    fun bindData(song: Song, view: View){
        itemView.run {
            txtNameSong.text = song.title

            itemView.setOnClickListener {
//                mediaPlayer = MediaPlayer.create(itemView.context, song.link)
//
//                mediaPlayer!!.start()

//                tvTotalTime = view.findViewById(R.id.tv_total_time)
//                tvCurrentTime = view.findViewById(R.id.tv_current_time)
//                seekbarController  = view.findViewById(R.id.seekbar_controller)
//
//                tvTotalTime.text = getTimeFormatted(mediaPlayer!!.duration)
//
//                tvCurrentTime.text =
//                    getTimeFormatted(mediaPlayer!!.currentPosition.toLong().toInt())
//
//                seekbarController.progress = mediaPlayer!!.currentPosition
//
//                playCycle(mediaPlayer, seekbarController, tvCurrentTime)

            }
        }
    }

//    private fun playCycle(mediaPlayer: MediaPlayer, seekbarController: SeekBar, tvCurrentTime: TextView) {
//        try {
//            seekbarController.progress = mediaPlayer!!.currentPosition
////            tvCurrentTime.text = getTimeFormatted(mediaPlayer!!.currentPosition.toLong().toInt())
////            tvCurrentTime.text = "toang" + mediaPlayer!!.currentPosition.toLong()
//            if (mediaPlayer!!.isPlaying) {
//                runnable = Runnable { playCycle(mediaPlayer, seekbarController, tvCurrentTime) }
//                handler.postDelayed(runnable, 100)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    private fun getTimeFormatted(milliSeconds: Int): String? {
//        var finalTimerString = ""
//        val secondsString: String
//
//        //Converting total duration into time
//        val hours = (milliSeconds / 3600000).toInt()
//        val minutes = (milliSeconds % 3600000).toInt() / 60000
//        val seconds = (milliSeconds % 3600000 % 60000 / 1000).toInt()
//
//        // Adding hours if any
//        if (hours > 0) finalTimerString = "$hours:"
//
//        // Prepending 0 to seconds if it is one digit
//        secondsString = if (seconds < 10) "0$seconds" else "" + seconds
//        finalTimerString = "$finalTimerString$minutes:$secondsString"
//
//        // Return timer String;
//        return finalTimerString
//    }
}